# ssg5-pandoc

Static Site Generator with pandoc (Forked from: https://www.romanzolotarev.com/ssg.html)

make a static site with find(1), grep(1), and lowdown or Markdown.pl
====================================================================

[ssg](/bin/ssg6) is a static site generator written in shell.

*ssg* converts markdown files to html with
[lowdown(1)](https://kristaps.bsd.lv/lowdown/) or
[Markdown.pl](https://daringfireball.net/projects/markdown/), copies
`*.html` files with `<HTML>` tag as they are. for the rest of `.html`
files *ssg* extracts their titles from `<H1>` tag, prepends
`_header.html`, appends `_footer.html`, copies from `src` to `dst`
directory, and generates `sitemap.xml`, ignores files with name `.*` or
listed in `src/.ssgignore`.

install
-------

lowdown(1) and Markdown.pl are required if there are `*.md` files.

On OpenBSD:

    $ mkdir -p bin
    $ ftp -Vo bin/ssg6 https://rgz.ee/bin/ssg6
    ssg6       100% |*********************|    4916      00:00
    $ chmod +x bin/ssg6
    $ doas pkg_add lowdown
    quirks-2.414 signed on 2018-03-28T14:24:37Z
    lowdown-0.3.1: ok
    $

Or on macOS:

    $ mkdir -p bin
    $ curl -s https://rgz.ee/bin/ssg6 > bin/ssg6
    $ curl -s https://rgz.ee/bin/Markdown.pl > bin/Markdown.pl
    $ chmod +x bin/ssg6 bin/Markdown.pl
    $

Usage {#Usage}
-----

Make sure `ssg6` and `lowdown` or `Markdown.pl` are in your `$PATH`:

    $ PATH="$HOME/bin:$PATH"
    $ mkdir src dst
    $ echo '# hello, world!' > src/index.md
    $ echo '<html><title></title>' > src/_header.html
    $ bin/ssg6 src dst 'test' 'http://www'
    ./index.md
    [ssg] 1 files, 1 url
    $ find dst
    dst
    dst/.files
    dst/index.html
    dst/sitemap.xml
    $ open dst/index.html

Markdown and HTML files {#Markdown%20and%20HTML%20files}
-----------------------

HTML files from `src` have priority over Markdown ones. *ssg* converts
Markdown files from `src` to HTML in `dst` and then copies HTML files
from `src` to `dst`. In the following example `src/a.html` wins:

    src/a.md   -> dst/a.html
    src/a.html -> dst/a.html

Incremental updates {#Incremental%20updates}
-------------------

On every run *ssg* saves a list of files in `dst/.files` and updates
only newer files. If no files were modified after that, *ssg* does
nothing.

    $ bin/ssg6 src dst 'Test' 'https://www'
    [ssg] no files, 1 url
    $

To force the update delete `dst/.files` and re-run *ssg*.

    $ rm dst/.files
    $ bin/ssg6 src dst 'Test' 'https://www'
    index.md
    [ssg] 1 file, 1 url
    $
